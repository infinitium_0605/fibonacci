import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 *  フィボナッチ数列出力用クラス
 *
 * @author Infinitium
 * @version 1.0.0
 */
public class Core {

    /**
     *
     * メインメソッド
     *
     * @param args 使用しません。
     */
    public static void main(String[] args){
        System.out.print("フィボナッチ数列 ver1.0.0 表示したい個数を入力してください。>");
        Scanner scanner = new Scanner(System.in);
        String inputTxt = scanner.nextLine();
        int inputNum;
        try{
            inputNum = Integer.parseInt(inputTxt);
        }catch(Exception ex){
            System.out.println("エラー:数字で入力してください。");
            main(args);
            return;
        }
        System.out.println("数列の出力を開始します。");
        List<Integer> fibonacci = getFibonacci(inputNum);
        Integer[] fibonacciArray = fibonacci.toArray(new Integer[fibonacci.size()]);
        System.out.println(Arrays.toString(fibonacciArray));
        System.out.println("数列の出力を終了します。");
    }

    /**
     *
     * 指定されたサイズのフィボナッチ数列の配列を返します。
     *
     * @param size 配列のサイズを指定します。
     * @return フィボナッチ数列のList<Integer>型
     */
    static List<Integer> getFibonacci(int size){
        List<Integer> fibonacciNums = new ArrayList<>();
        fibonacciNums.add(1);
        fibonacciNums.add(1);
        for(int i = 0;i < size;i++){
            if(i > 1){
                int num1 = fibonacciNums.get(i - 1);
                int num2 = fibonacciNums.get(i - 2);
                fibonacciNums.add(num1 + num2);
            }
        }
        return fibonacciNums;
    }
}
